; RUN: opt %loadPolly -polly-opt-isl -polly-tensor-contraction-opts=false \
; RUN: -debug < %s 2>&1| FileCheck %s
; RUN: opt %loadPolly -polly-opt-isl -polly-tensor-contraction-opts=true -debug < %s 2>&1| FileCheck %s --check-prefix=TC-PATTERN-MATCHING-OPTS
; RUN: opt %loadPolly -polly-opt-isl -polly-tensor-contraction-opts=true -stats -disable-output < %s 2>&1 | FileCheck %s --check-prefix=STATS -match-full-lines
; RUN: opt %loadPolly -polly-opt-isl -polly-tensor-contraction-opts=true \
; RUN: -polly-target-throughput-vector-fma=1 \
; RUN: -polly-target-latency-vector-fma=8 \
; RUN: -polly-target-1st-cache-level-associativity=8 \
; RUN: -polly-target-2nd-cache-level-associativity=8 \
; RUN: -polly-target-1st-cache-level-size=32768 \
; RUN: -polly-target-vector-register-bitwidth=256 \
; RUN: -polly-target-2nd-cache-level-size=262144 -analyze -polly-ast < %s \
; RUN: | FileCheck %s --check-prefix=TENSOR-CONTRACTION-OPTIMIZATION
; REQUIRES: asserts
;
;  for (int i = 0; i < 4000; i++)
;    for (int j = 0; j < 4000; j++)
;      for (int k = 0; k < 4000; ++k)
;        for (int l = 0; l < 4000; ++l)
;	  C[i][j][k] += A[j][l][i] * B[l][k];
;
; CHECK-NOT: The tensor contraction pattern was detected
; TC-PATTERN-MATCHING-OPTS: The tensor contraction pattern was detected
; STATS:  1 polly-opt-isl    - Number of tesnor contraction patterns detected
; TENSOR-CONTRACTION-OPTIMIZATION:    // Inter iteration alias-free
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:     // 1st level tiling - Tiles
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:     for (int c0 = 0; c0 <= 1; c0 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:       for (int c1 = 0; c1 <= 15; c1 += 1) {
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:         for (int c4 = 2048 * c0; c4 <= min(3999, 2048 * c0 + 2047); c4 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:           for (int c5 = 256 * c1; c5 <= min(3999, 256 * c1 + 255); c5 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:             CopyStmt_0(0, 0, c4, c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:         for (int c2 = 0; c2 <= 166666; c2 += 1) {
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:           if (c0 == 0)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:             for (int c3 = 3 * c2 / 125; c3 <= min(3999, (3 * c2 + 2) / 125); c3 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:               for (int c4 = max(0, 96 * c2 - 4000 * c3); c4 <= min(3999, 96 * c2 - 4000 * c3 + 95); c4 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                 for (int c6 = 256 * c1; c6 <= min(3999, 256 * c1 + 255); c6 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   CopyStmt_1(c3, c4, 0, c6);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:           // 1st level tiling - Points
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:           // Register tiling - Tiles
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:           for (int c3 = 0; c3 <= min(255, -256 * c0 + 499); c3 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:             for (int c4 = 0; c4 <= min(23, -24 * c2 + 3999999); c4 += 1)
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:               for (int c5 = 0; c5 <= min(255, -256 * c1 + 3999); c5 += 1) {
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                 // Loop Vectorizer Disabled
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                 // Register tiling - Points
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                 {
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 1, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 2, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 4, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 5, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 6, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000), 2048 * c0 + 8 * c3 + 7, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 1, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 2, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 4, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 5, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 6, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 1, 2048 * c0 + 8 * c3 + 7, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 1, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 2, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 4, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 5, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 6, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 2, 2048 * c0 + 8 * c3 + 7, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 1, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 2, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 3, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 4, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 5, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 6, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                   Stmt_for_body9((24 * c2 + c4) / 1000, 4 * ((24 * c2 + c4) % 1000) + 3, 2048 * c0 + 8 * c3 + 7, 256 * c1 + c5);
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:                 }
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:               }
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:         }
; TENSOR-CONTRACTION-OPTIMIZATION-NEXT:       }
;
; ModuleID = '<stdin>'
source_filename = "<stdin>"

define internal void @kernel(i32 %ni, i32 %nj, i32 %nk, i32 %nl, double %alpha, double %beta, [4000 x [4000 x double]]* %C, [4000 x [4000 x double]]* %A, [4000 x double]* %B) {
entry:
  br label %entry.split

entry.split:                                      ; preds = %entry
  br label %for.cond1.preheader

for.cond1.preheader:                              ; preds = %for.inc30, %entry.split
  %indvars.iv59 = phi i64 [ 0, %entry.split ], [ %indvars.iv.next60, %for.inc30 ]
  br label %for.cond4.preheader

for.cond4.preheader:                              ; preds = %for.inc27, %for.cond1.preheader
  %indvars.iv56 = phi i64 [ 0, %for.cond1.preheader ], [ %indvars.iv.next57, %for.inc27 ]
  br label %for.cond7.preheader

for.cond7.preheader:                              ; preds = %for.inc24, %for.cond4.preheader
  %indvars.iv53 = phi i64 [ 0, %for.cond4.preheader ], [ %indvars.iv.next54, %for.inc24 ]
  br label %for.body9

for.body9:                                        ; preds = %for.body9, %for.cond7.preheader
  %indvars.iv = phi i64 [ 0, %for.cond7.preheader ], [ %indvars.iv.next, %for.body9 ]
  %arrayidx13 = getelementptr inbounds [4000 x [4000 x double]], [4000 x [4000 x double]]* %A, i64 %indvars.iv56, i64 %indvars.iv, i64 %indvars.iv59
  %tmp = load double, double* %arrayidx13, align 8
  %arrayidx17 = getelementptr inbounds [4000 x double], [4000 x double]* %B, i64 %indvars.iv, i64 %indvars.iv53
  %tmp1 = load double, double* %arrayidx17, align 8
  %mul = fmul double %tmp, %tmp1
  %arrayidx23 = getelementptr inbounds [4000 x [4000 x double]], [4000 x [4000 x double]]* %C, i64 %indvars.iv59, i64 %indvars.iv56, i64 %indvars.iv53
  %tmp2 = load double, double* %arrayidx23, align 8
  %add = fadd double %tmp2, %mul
  store double %add, double* %arrayidx23, align 8
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 1
  %exitcond = icmp ne i64 %indvars.iv.next, 4000
  br i1 %exitcond, label %for.body9, label %for.inc24

for.inc24:                                        ; preds = %for.body9
  %indvars.iv.next54 = add nuw nsw i64 %indvars.iv53, 1
  %exitcond55 = icmp ne i64 %indvars.iv.next54, 4000
  br i1 %exitcond55, label %for.cond7.preheader, label %for.inc27

for.inc27:                                        ; preds = %for.inc24
  %indvars.iv.next57 = add nuw nsw i64 %indvars.iv56, 1
  %exitcond58 = icmp ne i64 %indvars.iv.next57, 4000
  br i1 %exitcond58, label %for.cond4.preheader, label %for.inc30

for.inc30:                                        ; preds = %for.inc27
  %indvars.iv.next60 = add nuw nsw i64 %indvars.iv59, 1
  %exitcond61 = icmp ne i64 %indvars.iv.next60, 4000
  br i1 %exitcond61, label %for.cond1.preheader, label %for.end32

for.end32:                                        ; preds = %for.inc30
  ret void
}
